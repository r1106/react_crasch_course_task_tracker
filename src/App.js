import './App.css';
import { useState, useEffect } from "react"
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Header from './components/Header';
import Tasks from './components/Tasks';
import AddTask from './components/AddTask';
import Footer from './components/Footer';
import About from './pages/About';

function App() {
  const [showAddTask, setShowAddTask] = useState(false);
  const [tasks, setTasks] = useState([]);

  useEffect(() => {
    const getTasks = async () => {
      const tasksFromServer = await fetchTasks();
      setTasks(tasksFromServer);
    };

    getTasks();
  }, [])


  const fetchTasks = async () => {
    const apiUrl = 'http://localhost:5000/tasks';
    const response = await fetch(apiUrl);
    const data = await response.json();

    return data;
  }

  const fetchTask = async (id) => {
    const apiUrl = `http://localhost:5000/tasks/${id}`;
    const response = await fetch(apiUrl);
    const data = await response.json();

    return data;
  }

  const addTask = async (task) => {
    const url = 'http://localhost:5000/tasks';
    const headers = {'Content-Type': 'application/json'};

    const response = await fetch(url, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(task),
    });

    const newTask = await response.json();
    setTasks([...tasks, newTask]);
  }

  const deleteTask = async (id) => {
    const url = `http://localhost:5000/tasks/${id}`;
    await fetch(url, {method: 'DELETE'});

    setTasks(tasks.filter((task) => task.id !== id));
  }

  const toggleTaskReminder = async (id) => {
    const taskToToggle = await fetchTask(id);
    const updatedTask = { ...taskToToggle, reminder: !taskToToggle.reminder }

    const url = `http://localhost:5000/tasks/${id}`;
    const headers = {'Content-Type': 'application/json'};

    const response = await fetch(url, {
      method: 'PUT',
      headers: headers,
      body: JSON.stringify(updatedTask),
    });

    const repondedTask = await response.json();
    setTasks(tasks.map(
      (task) => task.id === id
        ? { ...task, reminder: repondedTask.reminder } : task
    ));
    console.log('tasks', tasks);
  }

  return (
    <Router>
      <div className="container">
        <Header title={'Task Tracker'}
          showAddTaskForm={() => setShowAddTask(!showAddTask)}
          showAddTask={showAddTask}
        />

        <Route path="/" exact render={(props) => (
          <>
            {/* shorter way for writting: if ? then : else */}
            { showAddTask && <AddTask onAdd={addTask} /> }

            { tasks.length > 0
              ? <Tasks tasks={tasks} onDelete={deleteTask}
                onToggleReminder={toggleTaskReminder} />
              : 'No Task to show'
            }
          </>
        )} />

        <Route path="/about" component={About} />
        <Footer />
      </div>
    </Router>
  );
}

export default App;
