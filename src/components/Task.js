import PropTypes from 'prop-types'
import { FaTimes } from 'react-icons/fa'

const Task = ({id, text, day, reminder, onDelete, onToggleReminder}) => {
  return (
    <div onDoubleClick={() => onToggleReminder(id)}
      className={`task ${reminder ? 'reminder' : ''}`}>
      <h3>
        { text }
        <FaTimes style={ faTimesStyle } onClick={() => onDelete(id)} />
      </h3>
      <p>{ day }</p>
    </div>
  )
}

Task.defaultProps = {
  text: 'Todo',
  day: 'Month day todo text',
  reminder: false,
}

Task.protoType = {
  id: PropTypes.number,
  text: PropTypes.string,
  day: PropTypes.string,
  reminder: PropTypes.bool,
  onDelete: PropTypes.func,
  onToggleReminder: PropTypes.func,
}

const faTimesStyle = {
  color: 'red',
  cursor: 'pointer',
}

export default Task
