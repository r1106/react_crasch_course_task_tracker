import Task from "./Task"

const Tasks = ({ tasks, onDelete, onToggleReminder }) => {

  return (
    <div>
      {tasks.map((task) => (
        <Task key={task.id} id={task.id} text={task.text} day={task.day}
          reminder={task.reminder} onDelete={onDelete}
          onToggleReminder={onToggleReminder}
        />
      ))}
    </div>
  )
}

export default Tasks
